-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 04, 2014 at 02:45 PM
-- Server version: 5.5.40-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `f4904293_techno`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `content` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `name`, `content`, `created`, `modified`, `post_id`, `user_id`) VALUES
(1, 'comment 1', 'comment 1 for first problem..', '2014-11-27 03:23:23', '2014-11-27 03:23:23', 2, 1),
(2, 'comment 2', 'comment 2 for first problem', '2014-11-27 03:23:40', '2014-11-27 03:23:40', 2, 1),
(3, 'comment 1', 'comment 1 for second problem', '2014-11-27 03:24:06', '2014-11-27 03:24:06', 3, 1),
(4, 'comment 2', 'comment 2 for second problem', '2014-11-27 03:24:34', '2014-11-27 03:24:34', 3, 1),
(5, 'Awesome', 'tesing comment for second problem', '2014-11-28 05:23:15', '2014-11-28 05:23:15', 2, 1),
(6, 'Excellent', 'My comments for this post', '2014-11-28 05:32:51', '2014-11-28 05:32:51', 3, 1),
(7, 'User Id testing', 'I have to check user id', '2014-11-28 06:30:23', '2014-11-28 06:30:23', 3, 1),
(8, 'Done', 'Excellent job by user id', '2014-11-28 06:32:53', '2014-11-28 06:32:53', 2, 2),
(9, 'Wow', 'Simply awesome\r\n', '2014-12-02 03:19:39', '2014-12-02 03:19:39', 2, 2),
(10, 'Excellent', 'Its very helpful', '2014-12-02 19:00:42', '2014-12-02 19:00:42', 14, 19),
(11, 'Marvellous', 'Its very useful', '2014-12-02 19:04:25', '2014-12-02 19:04:25', 14, 19),
(12, 'Hard one', 'Its a hard one', '2014-12-04 05:31:03', '2014-12-04 05:31:03', 16, 1),
(13, 'Good Job ', 'Its help me', '2014-12-04 17:51:17', '2014-12-04 17:51:17', 14, 25);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `solution` text,
  `solution_date` datetime DEFAULT NULL,
  `filename` varchar(150) DEFAULT NULL,
  `dir` text,
  `last_login` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `size` int(4) unsigned DEFAULT NULL,
  `mime` varchar(50) DEFAULT NULL,
  `path` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `name`, `content`, `solution`, `solution_date`, `filename`, `dir`, `last_login`, `created`, `modified`, `user_id`, `size`, `mime`, `path`) VALUES
(14, 'File Search', 'Write a C# .NET Windows application or Console Application as per following:\r\n\r\nCreate a text file â€“ numbers.txt to store 10 numbers in it Create it through the application or separately outside the application. Then using that file read the numbers and find out the largest number.\r\n', 'Please download the zip file', '2014-12-01 02:59:00', 'SearchFile.zip', '', NULL, '2014-12-02 15:03:14', '2014-12-02 15:03:14', 10, 69034, 'application/octet-stream', '/files/SearchFile.zip'),
(16, 'Creating array and functions', 'You have to write down the code for the functions. This is the code for you:\r\n<?php\r\n\r\nfunction make_array($nElements,$min,$max)\r\n{\r\n   \r\n}\r\n\r\nfunction getElement($array,$index)\r\n{\r\n	\r\n}\r\n\r\n\r\nfunction getLastElement($array)\r\n{\r\n	\r\n}\r\n\r\n\r\nfunction getMin($array)\r\n{\r\n	\r\n}\r\n\r\nfunction insert(&$array,$value)\r\n{\r\n	\r\n}\r\n\r\nfunction swap(&$array,$from,$to)\r\n{\r\n	\r\n}\r\n\r\nfunction print_array($array)\r\n{\r\n	\r\n}\r\n\r\n\r\n\r\n//////////// DO NOT TOUCH AFTER THIS LINE\r\n$_nE = 10;\r\n$_min = -10;\r\n$_max = 100;\r\n$_to = rand(0,$_nE - 1);\r\n$_from = rand(0,$_nE - 1);\r\n$_index = rand(0,$_nE);\r\necho ''<pre>'';\r\n$array = make_array($_nE, $_min, $_max);\r\nprint_array($array);\r\necho ''<hr> Insert Element<hr>'';\r\ninsert($array, ''<span style="color:red">comp1230</span>'');\r\nprint_array($array);\r\necho ''<hr> Swap Elements (from : '' . $_from . '' , to: '' . $_to . '')<hr>'';\r\nswap($array,$_from,$_to);\r\nprint_array($array);\r\necho ''<hr> Min value'';\r\necho ''Min value is : '' . getMin($array);\r\necho ''<hr>'';\r\necho ''<hr>Get Element By Index: ['' . $_index . ''] : '';\r\necho getElement($array, $_index);\r\necho ''<hr>Get Last element vlaue: '';\r\necho getLastElement($array);', '<?php\r\n\r\nfunction make_array($nElements,$min,$max)\r\n{\r\n   $myArray = array();\r\n   for($i=0;$i<$nElements;$i++){\r\n		$myArray[]=rand($min,$max);\r\n   }\r\n   return $myArray;\r\n}\r\n\r\nfunction getElement($array,$index)\r\n{\r\n	return $array[$index];\r\n}\r\n\r\n\r\nfunction getLastElement($array)\r\n{\r\n	return getElement($array,count($array)-1);\r\n	//return $array[count($array)-1];\r\n}\r\n\r\n\r\nfunction getMin($array)\r\n{\r\n	$min = $array[0];\r\n	\r\n	foreach($array as $key=>$value){\r\n		if($min<$value){\r\n			$min = $value;\r\n		}\r\n	}\r\n	\r\n	return $min;\r\n}\r\n\r\nfunction insert(&$array,$value)\r\n{\r\n	$array[]=$value;\r\n	//array_push($array,$value);\r\n	//$array[count($array)]=$value;\r\n}\r\n\r\nfunction swap(&$array,$from,$to)\r\n{\r\n	$temp = $array[$from];\r\n	$array[$from] = $array[$to];\r\n	$array[$to]=$temp;\r\n}\r\n\r\nfunction print_array($array)\r\n{\r\n	//print_r ($array);\r\n	foreach($array as $key=>$value){\r\n		echo ''[''.$key. ''] = '' .$value.''<br>'';\r\n	}\r\n}\r\n\r\n\r\n\r\n//////////// DO NOT TOUCH AFTER THIS LINE\r\n$_nE = 10;\r\n$_min = -10;\r\n$_max = 100;\r\n$_to = rand(0,$_nE - 1);\r\n$_from = rand(0,$_nE - 1);\r\n$_index = rand(0,$_nE);\r\necho ''<pre>'';\r\n$array = make_array($_nE, $_min, $_max);\r\nprint_array($array);\r\necho ''<hr> Insert Element<hr>'';\r\ninsert($array, ''<span style="color:red">comp1230</span>'');\r\nprint_array($array);\r\necho ''<hr> Swap Elements (from : '' . $_from . '' , to: '' . $_to . '')<hr>'';\r\nswap($array,$_from,$_to);\r\nprint_array($array);\r\necho ''<hr> Min value'';\r\necho ''Min value is : '' . getMin($array);\r\necho ''<hr>'';\r\necho ''<hr>Get Element By Index: ['' . $_index . ''] : '';\r\necho getElement($array, $_index);\r\necho ''<hr>Get Last element vlaue: '';\r\necho getLastElement($array);', '2014-12-01 15:26:00', 'lab2b.php', '', NULL, '2014-12-02 15:30:31', '2014-12-02 15:30:31', 10, 1633, 'application/octet-stream', '/files/lab2b.php'),
(17, 'C# Console application with user input', '1.	Create simple console application that will display a numbered list of available dog  names and let the user choose the name by entering the number of the name. The chosen name will be then written to console.\r\n\r\nList to display:\r\n1 - Max\r\n2 - Baily\r\n3 - Buddy\r\n4 - Molly\r\n5 - Maggie\r\n6 - Daisy\r\n7 - Bella\r\n8 - Jake\r\n9 - Rocky\r\n10 - Lucy\r\n\r\nInput : Number of name\r\nOutput : Your dog''s name will be ...\r\n\r\nNeeded skills:\r\n\r\nif/else or switch statement\r\n', '', '2014-12-05 16:51:00', 'Solution for  C# Console application with user input.docx', '', NULL, '2014-12-02 16:57:49', '2014-12-04 05:29:07', 9, 12910, 'application/vnd.openxmlformats-officedocument.word', '/files/Solution for  C# Console application with user input.docx'),
(18, 'Create propoerties of Class', 'Create the table first:\r\nCREATE TABLE IF NOT EXISTS `pageviews` (\r\n  `id` int(10) unsigned DEFAULT NULL,\r\n  `page_id` int(11) NOT NULL,\r\n  `views` int(11) NOT NULL\r\n)\r\n\r\n\r\nINSERT INTO `pageviews` (`id`, `page_id`, `views`) VALUES\r\n(NULL, 1, 1);\r\n\r\nThen in the following codes implement the methods:\r\n', '<?php\r\n\r\n   define(''PAGE_ID'', 1);\r\n   define(''DEBUG'', false);\r\n\r\n   final class PageViews\r\n   {\r\n      private $con;\r\n\r\n      public function  __construct()\r\n      {\r\n         // connect to database and save the conection object to $con.\r\n      }\r\n\r\n      public function savePageView()\r\n      {\r\n         // use the PAGE_ID to identify the record\r\n      }\r\n      public function getPageView()\r\n      {\r\n          // get pageView (views) using PAGE_ID\r\n\r\n      }\r\n    }\r\n\r\n   ////////// DONT TOUCH AFTER THIS LINE//////\r\n\r\n   $page = new PageViews();\r\n   $views =   $page->getPageView();\r\n   $page->savePageView();\r\n\r\n?>\r\n<!DOCTYPE html>\r\n<html lang=''en''>\r\n   <head>\r\n     <title>Lab 4 : <?php echo $views;?></title>\r\n   </head>\r\n   <body>\r\n     Total Page view : <?php echo $views;?>\r\n   </body>\r\n</html>', '2014-12-01 16:56:00', 'index.php', '', NULL, '2014-12-02 17:00:50', '2014-12-02 17:00:50', 10, 1643, 'application/octet-stream', '/files/index.php'),
(19, 'Create a function to reverse a string', 'Create a PHP function to reverse a string', 'download solution', '2014-12-02 04:22:00', 'function.php', '', NULL, '2014-12-04 04:24:06', '2014-12-04 04:24:06', 1, 277, 'application/octet-stream', '/files/function.php'),
(20, 'PHP Information', 'Write down a script that shows the php information of your server', '<?php\r\n\r\nphpinfo();\r\n\r\n?>', '2014-12-02 17:52:00', 'Chrysanthemum.jpg', '', NULL, '2014-12-04 17:53:53', '2014-12-04 18:00:52', 10, 879394, 'image/jpeg', '/files/Chrysanthemum.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `posts_tags`
--

CREATE TABLE IF NOT EXISTS `posts_tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(11) unsigned DEFAULT NULL,
  `tag_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `posts_tags`
--

INSERT INTO `posts_tags` (`id`, `post_id`, `tag_id`) VALUES
(14, 14, 4),
(15, 14, 9),
(16, 14, 10),
(20, 16, 1),
(21, 17, 4),
(22, 17, 9),
(23, 17, 10),
(24, 18, 1),
(25, 18, 12),
(26, 19, 1),
(27, 20, 1),
(28, 20, 15);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'student'),
(2, 'peer tutor'),
(3, 'experts'),
(4, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `longname` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `longname`) VALUES
(1, 'PHP', 'PHP Programming'),
(2, 'CakePHP', 'CakePHP Framework'),
(3, 'PHP Framework', 'PHP Framework'),
(4, 'C#', 'C# Programming Language'),
(5, 'HTML', 'HTML'),
(6, 'JavaScript', 'JavaScript'),
(7, 'CSS', 'CSS'),
(8, 'jQuery', 'jQuery'),
(9, 'OOP', 'Object Oriented Programming'),
(10, 'Exception Handling', 'Exception Handling'),
(11, 'Database', 'Database'),
(12, 'MySQL', 'MySQL'),
(13, 'System Analysis', 'System Analysis'),
(14, 'Java', 'Java'),
(15, 'ex', 'Exception Handling');

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE IF NOT EXISTS `uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `size` int(4) unsigned NOT NULL,
  `mime` varchar(50) NOT NULL,
  `path` text NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `post_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `name`, `size`, `mime`, `path`, `created`, `modified`, `post_id`) VALUES
(23, 'IMG_20140914_154025.jpg', 108835, 'image/jpeg', '/files/IMG_20140914_154025.jpg', '2014-11-29 06:17:51', '2014-11-29 06:17:51', 3),
(24, 'Photos.zip', 108431, 'application/octet-stream', '/files/Photos.zip', '2014-11-29 06:19:25', '2014-11-29 06:19:25', 3),
(25, 'LizPopCulturecourseoutlineFALL2014 (1).doc', 126464, 'application/msword', '/files/LizPopCulturecourseoutlineFALL2014 (1).doc', '2014-11-30 18:09:07', '2014-11-30 18:09:07', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `firstName` varchar(15) NOT NULL,
  `lastName` varchar(15) NOT NULL,
  `student_id` varchar(12) NOT NULL,
  `program_code` varchar(5) NOT NULL,
  `role` varchar(64) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `firstName`, `lastName`, `student_id`, `program_code`, `role`, `created`, `modified`, `status`) VALUES
(1, 'nasim789', '30971f81083e5c8189ddd72b08afabf7cd076612', 'nasim789@gmail.com', '', '', '', '', 'admin', '2014-11-26 07:41:50', '2014-11-26 07:41:50', 1),
(2, 'test1', '89c83fe2130ac25957d5928390c616f629492344', 'test@yahoo.com', '', '', '', '', 'student', '2014-11-26 08:12:59', '2014-11-26 08:12:59', 1),
(3, 'test2', '89c83fe2130ac25957d5928390c616f629492344', 'test@yahoo.com', '', '', '', '', 'student', '2014-11-26 08:13:44', '2014-11-26 08:13:44', 1),
(9, 'tsegai', '89c83fe2130ac25957d5928390c616f629492344', 't@gmail.com', '', '', '', '', 'admin', '2014-11-29 06:58:58', '2014-11-29 06:58:58', 1),
(10, 'manir', '89c83fe2130ac25957d5928390c616f629492344', 'manir@gmail.com', '', '', '', '', 'peertutor', '2014-12-02 04:35:41', '2014-12-02 04:35:41', 1),
(11, 'nasimul', '89c83fe2130ac25957d5928390c616f629492344', 'nasim789@gmail.com', '', '', '', '', 'student', '2014-12-02 07:40:29', '2014-12-02 07:40:29', 1),
(18, 'lastone', '89c83fe2130ac25957d5928390c616f629492344', 'nasim789@gmail.com', '', '', '', '', 'student', '2014-12-02 07:21:28', '2014-12-02 07:21:28', 1),
(19, 'Bigboss', '89c83fe2130ac25957d5928390c616f629492344', 'nasim789@gmail.com', '', '', '', '', 'student', '2014-12-02 07:23:49', '2014-12-04 17:56:05', 0),
(20, 'sean2014', '89c83fe2130ac25957d5928390c616f629492344', 'xsi1993109@hotmail.ca', '', '', '', '', 'student', '2014-12-03 21:27:47', '2014-12-04 03:14:22', 1),
(21, 'student1', '89c83fe2130ac25957d5928390c616f629492344', 'student1@gmail.com', '', '', '', '', NULL, '2014-12-04 03:16:24', '2014-12-04 03:16:24', 1),
(22, 'student2', '89c83fe2130ac25957d5928390c616f629492344', 'nasim789@gmail.com', '', '', '', '', NULL, '2014-12-04 03:25:14', '2014-12-04 03:25:14', 1),
(23, 'student3', '89c83fe2130ac25957d5928390c616f629492344', 'nasim789@gmail.com', '', '', '', '', 'student', '2014-12-04 03:50:19', '2014-12-04 03:50:19', 1),
(24, 'maziar', 'e0ecaaf4aec98738cee57df483d56676c4cba35d', 'mmasoudi@georgebrown.ca', '', '', '', '', 'student', '2014-12-04 17:46:34', '2014-12-04 17:46:34', 1),
(25, 'marlyn', '89c83fe2130ac25957d5928390c616f629492344', 'marlynperez@gmail.com', '', '', '', '', 'student', '2014-12-04 17:49:25', '2014-12-04 17:49:25', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
