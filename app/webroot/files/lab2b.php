<?php

function make_array($nElements,$min,$max)
{
   $myArray = array();
   for($i=0;$i<$nElements;$i++){
		$myArray[]=rand($min,$max);
   }
   return $myArray;
}

function getElement($array,$index)
{
	return $array[$index];
}


function getLastElement($array)
{
	return getElement($array,count($array)-1);
	//return $array[count($array)-1];
}


function getMin($array)
{
	$min = $array[0];
	
	foreach($array as $key=>$value){
		if($min<$value){
			$min = $value;
		}
	}
	
	return $min;
}

function insert(&$array,$value)
{
	$array[]=$value;
	//array_push($array,$value);
	//$array[count($array)]=$value;
}

function swap(&$array,$from,$to)
{
	$temp = $array[$from];
	$array[$from] = $array[$to];
	$array[$to]=$temp;
}

function print_array($array)
{
	//print_r ($array);
	foreach($array as $key=>$value){
		echo '['.$key. '] = ' .$value.'<br>';
	}
}



//////////// DO NOT TOUCH AFTER THIS LINE
$_nE = 10;
$_min = -10;
$_max = 100;
$_to = rand(0,$_nE - 1);
$_from = rand(0,$_nE - 1);
$_index = rand(0,$_nE);
echo '<pre>';
$array = make_array($_nE, $_min, $_max);
print_array($array);
echo '<hr> Insert Element<hr>';
insert($array, '<span style="color:red">comp1230</span>');
print_array($array);
echo '<hr> Swap Elements (from : ' . $_from . ' , to: ' . $_to . ')<hr>';
swap($array,$_from,$_to);
print_array($array);
echo '<hr> Min value';
echo 'Min value is : ' . getMin($array);
echo '<hr>';
echo '<hr>Get Element By Index: [' . $_index . '] : ';
echo getElement($array, $_index);
echo '<hr>Get Last element vlaue: ';
echo getLastElement($array);