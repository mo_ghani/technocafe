<?php

   define('PAGE_ID', 1);
   define('DEBUG', false);

   final class PageViews
   {
      private $con;

      public function  __construct()
      {
         // connect to database and save the conection object to $con.
        $username = "root";
        $password = "";

        $this->con = new PDO("mysql:host=localhost;dbname=test", $username, $password); //f4904293_test
            
      }

      public function savePageView()
      {
         // use the PAGE_ID to identify the record
        $qry = 'update pageviews set views=views+1 where page_id='.PAGE_ID;
        $stmt = $this->con->prepare($qry);

        // execute the query
        $stmt->execute();

      }
      public function getPageView()
      {
        // get pageView (views) using PAGE_ID
        $sql = "select views from pageviews where page_id = ".PAGE_ID;
        
		/* using prepare statement
		
		$stmt = $this->con->prepare($sql); 
        $stmt->execute();
        $row = $stmt->fetchAll(PDO::FETCH_ASSOC); 
		
		*/
		
		///////////////one way to do this ///////////////////////
        $result = $this->con->query($sql);
		$row = $result->fetch();
		/////////////////////////////////////////////////////////////////
		//echo $row["views"];
		
		//return $row[0]["views"]; //works for the first one
		return $row[0];
      }
    }

   ////////// DONT TOUCH AFTER THIS LINE//////

   $page = new PageViews();
   $views = $page->getPageView();
   $page->savePageView();

?>
<!DOCTYPE html>
<html lang='en'>
   <head>
     <title>Lab 4 : <?php echo $views;?></title>
   </head>
   <body>
     Total Page view : <?php echo $views;?>
   </body>
</html>
