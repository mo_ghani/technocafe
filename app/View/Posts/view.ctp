<?php
    App::import('Controller','Users');
?>
<div class="posts view">
<h2><?php echo h($post['Post']['name']); ?></h2>
<h4>Posted By: <?php echo $this->Html->link($post['User']['username'], array('controller' => 'users', 'action' => 'view', $post['User']['id'])); ?> 
| On <?php echo h(date('M d, Y',strtotime($post['Post']['created']))); ?></h4></br>
    <dl>
        <dt><?php echo __('Description'); ?></dt>
        <dd>
                <?php echo h($post['Post']['content']); ?>
                &nbsp;
        </dd>
        <?php if($post['Post']['solution_date']<=date("Y-m-d")) { ?>
        <dt><?php echo __('Solution'); ?></dt>
        <dd>
                <?php echo h($post['Post']['solution']); ?>
                &nbsp;
        </dd>
        <?php if($post['Post']['filename']!=''){ ?>
        <dt><?php echo __('Uploaded File') ?></dt>
        <dd>
            <?php 
                
                echo $this->Html->link($post['Post']['filename'], $post['Post']['path'], array('target' => '_blank'));
                
            ?>
        </dd>
        <?php } ?>
        <?php }else{ ?>
        <dt><?php echo __('Solution\'ll be on'); ?></dt>
        <dd>
                <?php echo h(date('M d Y',strtotime($post['Post']['solution_date']))); ?>
                &nbsp;
        </dd>
        <?php } ?>
        
    </dl>
    
</div>
<?php echo $this->element('menu');?>

<div class="related">
	<h3><?php echo __('Comments'); ?></h3>
        <hr>
	<?php if (!empty($post['Comment'])): ?>
        <div>
	<?php foreach ($post['Comment'] as $comment): ?>
            
                <b><?php echo $comment['name']; ?></b> 
            <h6>
                <?php 
                $controller = new UsersController;
                $uname = $controller->getUsernameById($comment['user_id']);
                echo $uname['User']['username'];
                ?>
            </h6>
                <p>
                <?php echo $comment['content']; ?>
                </p>
	<?php endforeach; ?>
        </div>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('Add Comment'), array('controller' => 'comments', 'action' => 'add',$post['Post']['id'])); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
    <h3><?php echo __('Related Tags'); ?></h3><hr>
	<?php if (!empty($post['Tag'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	
	<?php 
            foreach ($post['Tag'] as $tag): 
                echo '<span style="background-color:gray">'.$tag['name'].'</span>&nbsp;';
            endforeach; 
        ?>
	</table>
<?php endif; ?>

</div>