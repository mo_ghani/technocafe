<?php
    App::import('Controller','Tags');
    
    $controller = new TagsController;
    $tags = $controller->getAll();
?>
<div id='main'>
    <div id="col1">
        <p><b>Tags</b>
            <?php //debug($controller->getAll());?>
        </p>
        <p>
        <ul>
            <?php foreach ($tags as $tag): ?>
            <?php 
                echo '<li>[ '.$this->Html->link(h($tag['Tag']['name']),array('controller'=>'tags','action'=>'view',h($tag['Tag']['id']))).' ]</li>';
                //echo '<span style="background-color:white">'.h($tag['Tag']['name']).'</span>&nbsp;';//echo h($tag['Tag']['name']); ?>
            <?php endforeach;?>
        </ul>
        </p> 
        <?php //echo $this->element('menu');?> 
    </div> 
    <div id="col2outer"> 
            <div id="col2mid"> 
                    <p>
                        <?php echo $this->Html->image('title1.png', array('alt' => 'TechnoCafe', 'border' => '0','width'=>'auto'));?>
                    </p> 
                    <h3>Welcome</h3>
                    <p>
                    TechnoCafe helps to teach you programming languages in problem solving way. We will upload problems on different programming languages and you will try 
                    to solve it. Thus you will learn about programming. After a certain time we will provide the solution for you. So that you can check with your solution and
                    you can fix your errors.
                    </p> 
            </div> 
            <div > 
                <?php echo $this->element('menu');?> 
                
            </div>
    </div>
</div>