<div class="posts index">
	<h2><?php echo __('Problems'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
<!--			<th><?php //echo $this->Paginator->sort('id'); ?></th>-->
			<th><?php echo $this->Paginator->sort('title'); ?></th>
			<th><?php echo $this->Paginator->sort('Description'); ?></th>
<!--			<th><?php //echo $this->Paginator->sort('solution'); ?></th>-->
			<th><?php echo $this->Paginator->sort('solution posted'); ?></th>
<!--			<th><?php //echo $this->Paginator->sort('last_login'); ?></th>-->
			<th><?php echo $this->Paginator->sort('published'); ?></th>
<!--			<th><?php //echo $this->Paginator->sort('modified'); ?></th>-->
			<th><?php echo $this->Paginator->sort('published by'); ?></th>
			<?php if(AuthComponent::user('role')!='student'){ ?>
                        <th class="actions"><?php echo __('Actions'); ?></th>
                        <?php } ?>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($posts as $post): ?>
	<tr>
<!--		<td><?php //echo h($post['Post']['id']); ?>&nbsp;</td>-->
		<td><?php echo $this->Html->link(h($post['Post']['name']), array('action' => 'view', $post['Post']['id'])); ?>&nbsp;</td>
                <td><?php if(strlen(h($post['Post']['content']))>120){echo substr(h($post['Post']['content']), 0,120).' '.$this->Html->link('..>>', array('action' => 'view', $post['Post']['id']));}else{echo h($post['Post']['content']);}; ?>&nbsp;</td>
<!--		<td><?php //echo h($post['Post']['solution']); ?>&nbsp;</td>-->
                <td><?php if ($post['Post']['solution_date']<=date("Y-m-d")) echo "<span style='color:green'>Yes</span>"; else echo "<span style='color:red'>No</span>"; ?>&nbsp;</td>
<!--		<td><?php //echo h($post['Post']['last_login']); ?>&nbsp;</td>-->
		<td><?php echo h(date('M d, Y',strtotime($post['Post']['created']))); ?>&nbsp;</td>
<!--		<td><?php //echo h($post['Post']['modified']); ?>&nbsp;</td>-->
		<td>
			<?php echo $this->Html->link($post['User']['username'], array('controller' => 'users', 'action' => 'view', $post['User']['id'])); ?>
		</td>
		<td class="actions">
			<?php if(AuthComponent::user('role')!='student'){ ?>
                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $post['Post']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $post['Post']['id']), array(), __('Are you sure you want to delete # %s?', $post['Post']['id'])); ?>
                        <?php } ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu');?>
