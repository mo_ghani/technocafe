<div class="posts form">
<?php echo $this->Form->create('Post'); ?>
	<fieldset>
		<legend><?php echo __('Edit Post'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name',array('label'=>'Title'));
		echo $this->Form->input('content',array('label'=>'Description'));
		echo $this->Form->input('solution');
		echo $this->Form->input('solution_date',array('label'=>'Solution Available Date'));
                //echo $this->Form->file('file',array('type'=>'file','label'=>'Upload File'));
                //echo $this->Form->input('dir',array('type'=>'hidden'));
		//echo $this->Form->input('last_login');
		echo $this->Form->input('user_id',array('type'=>'hidden'));
		echo $this->Form->input('Tag');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu');?>
