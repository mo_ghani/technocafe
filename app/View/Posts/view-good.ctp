<?php
    App::import('Controller','Users');
?>
<div class="posts view">
<h2><?php echo h($post['Post']['name']); ?></h2>
<h4>Posted By: <?php echo $this->Html->link($post['User']['username'], array('controller' => 'users', 'action' => 'view', $post['User']['id'])); ?> 
| On <?php echo h(date('M d, Y',strtotime($post['Post']['created']))); ?></h4></br>
    <dl>
        <dt><?php echo __('Description'); ?></dt>
        <dd>
                <?php echo h($post['Post']['content']); ?>
                &nbsp;
        </dd>
        <?php if($post['Post']['solution_date']<=date("Y-m-d")) { ?>
        <dt><?php echo __('Solution'); ?></dt>
        <dd>
                <?php echo h($post['Post']['solution']); ?>
                &nbsp;
        </dd>
        <?php }else{ ?>
        <dt><?php echo __('Solution On'); ?></dt>
        <dd>
                <?php echo h(date('M d Y',strtotime($post['Post']['solution_date']))); ?>
                &nbsp;
        </dd>
        <?php } ?>
        <dt><?php echo __('Uploaded File') ?></dt>
        <dd>
            <ul>
                <?php foreach ($post['Upload'] as $upload): ?>
                    <li><?php echo $this->Html->link($upload['name'], $upload['path'], array('target' => '_blank'));?></li>
                <?php endforeach; ?>
            </ul>
        </dd>
    </dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Posts'), array('action' => 'index')); ?> </li>
		
	</ul>
</div>
<hr>
<div id="comments">
    <?php
        //$this->Ajax->form('/comments/add','post',array('update'=>'comments'));
    ?>
    <?php //echo $this->Js->input('Comment.name');?> 
    <?php //echo $this->Js->input('Comment.content');?> 
    <?php //echo $this->Js->input('Comment.post_id',array('type'=>'hidden','value'=>$post['Post']['id']));?> 
    <?php //echo $this->Js->end('Add Comment');?>
    
</div>
<hr>
<div class="related">
	<h3><?php echo __('Comments'); ?></h3>
        <hr>
	<?php if (!empty($post['Comment'])): ?>
        <div>
	<?php foreach ($post['Comment'] as $comment): ?>
            <p><b><?php echo $comment['name']; ?></b>,&nbsp; <?php 
                $controller = new UsersController;
                $uname = $controller->getUsernameById($comment['user_id']);
                echo $uname['User']['username'];
            ?></p>
            <p><?php echo $comment['content']; ?></p>
	<?php endforeach; ?>
        </div>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('Add Comment'), array('controller' => 'comments', 'action' => 'add',$post['Post']['id'])); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
    <h3><?php echo __('Related Tags'); ?></h3><hr>
	<?php if (!empty($post['Tag'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	
	<?php 
            foreach ($post['Tag'] as $tag): 
                echo '<span style="background-color:gray">'.$tag['name'].'</span>&nbsp;';
            endforeach; 
        ?>
	</table>
<?php endif; ?>

</div>
<?php echo $this->Js->writeBuffer();?>