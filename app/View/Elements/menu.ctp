<?php 
//debug($this->Session->read('Auth.User'));
$user = $this->Session->read('Auth.User');
if($this->Session->read('Auth.User')){ 
?>
<h3><?php echo 'Hi, '.AuthComponent::user('username'); ?></h3>
<div class="actions">
    <ul>
        <?php if(AuthComponent::user('role')!='student'){ ?>
            <li><?php echo $this->Html->link(__('Problem List'), array('controller'=>'posts','action' => 'index')); ?></li>
            <li><?php echo $this->Html->link(__('My Posted Problems'), array('controller'=>'posts','action' => 'myposts',AuthComponent::user('id'))); ?></li>
            <li><?php echo $this->Html->link(__('New Problem'), array('action' => 'add')); ?></li>
            <?php if(AuthComponent::user('role')=='admin'){ ?>
            <li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
            <li><?php echo $this->Html->link(__('List Comments'), array('controller' => 'comments', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('List Uploads'), array('controller' => 'uploads', 'action' => 'index')); ?> </li>
            <?php } ?>
            <li><?php echo $this->Html->link(__('List Tags'), array('controller' => 'tags', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('New Tag'), array('controller' => 'tags', 'action' => 'add')); ?> </li>
            <li><?php echo $this->Html->link( "Logout",   array('controller'=>'users','action'=>'logout') );  ?></li>
        <?php }else{ ?>
            <li><?php echo $this->Html->link(__('Problem List'), array('controller'=>'posts','action' => 'index')); ?></li>
            <li><?php echo $this->Html->link(__('List Tags'), array('controller' => 'tags', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link( "Logout",   array('controller'=>'users','action'=>'logout') );  ?></li>
        <?php } ?>
    </ul>
</div>
<?php }else{ ?>
    <p>
        <?php echo $this->Html->link(__('Login'), array('controller' => 'users', 'action' => 'login')); ?> 
        <?php echo 'New User? '.$this->Html->link(__('Sign Up'), array('controller' => 'users', 'action' => 'add')); ?> 
    </p>
    
<?php } ?>