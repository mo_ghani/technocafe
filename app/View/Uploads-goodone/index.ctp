<div class="uploads index">
  <p>
    <?php echo $this->Html->link(__('Upload'), array('action' => 'upload')); ?>
  </p>
  <table>
    <tr>
      <th><?php echo __('Name'); ?></th>
      <th><?php echo __('Uploaded'); ?></th>
    </tr>
    <?php foreach ($uploads as $upload): ?>
    <tr>
      <td>
        <?php
          echo $this->Html->link($upload['Upload']['name'], $upload['Upload']['path'], array('target' => '_blank'));
        ?>
      </td>
      <td>
        <?php
          echo $this->Time->nice($upload['Upload']['created']);
        ?>
      </td>
    </tr>
    <?php endforeach; ?>
  </table>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Upload'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Posts'), array('controller' => 'posts', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Post'), array('controller' => 'posts', 'action' => 'add')); ?> </li>
	</ul>
</div>
