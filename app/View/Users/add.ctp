<!-- app/View/Users/add.ctp -->
<div class="users form">

<?php echo $this->Form->create('User');?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
        <?php echo $this->Form->input('username');
		echo $this->Form->input('email');
        echo $this->Form->input('password');
	echo $this->Form->input('password_confirm', array('label' => 'Confirm Password *', 'maxLength' => 255, 'title' => 'Confirm password', 'type'=>'password'));
        //echo $this->Form->input('role',array('value'=>'student','hiddenField'=>TRUE));
        if(AuthComponent::user('role')=='admin'){
            echo $this->Form->input('role', array(
                'options' => array( 'student' => 'Student', 'peertutor' => 'Peer Tutor','admin'=>'Admin')
            ));
        }else{
            $this->Form->input('role',array('type'=>'hidden','value'=>'student'));
        }
		
	echo $this->Form->submit('Registration', array('class' => 'form-submit',  'title' => 'Click here to add the user') ); 
?>
    </fieldset>
<?php echo $this->Form->end(); ?>
</div>
<?php echo $this->element('menu');?>