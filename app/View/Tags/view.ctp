<div class="tags view">
<h2><?php echo __('Tag'); ?></h2>
	<dl>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($tag['Tag']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Longname'); ?></dt>
		<dd>
			<?php echo h($tag['Tag']['longname']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php echo $this->element('menu');?>
<div class="related">
	<h3><?php echo __('Related Posts'); ?></h3>
	<?php if (!empty($tag['Post'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Content'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($tag['Post'] as $post): ?>
		<tr>
			<td><?php echo $post['name']; ?></td>
			<td><?php if(strlen(h($post['content']))>120){echo substr(h($post['content']), 0,120).' '.$this->Html->link('..>>', array('controller'=>'posts','action' => 'view', $post['id']));}else{echo h($post['content']);}; ?></td>
			<td><?php echo $post['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'posts', 'action' => 'view', $post['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
