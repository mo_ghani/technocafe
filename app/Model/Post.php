<?php
App::uses('AppModel', 'Model');
/**
 * Post Model
 *
 * @property User $User
 * @property Comment $Comment
 * @property Upload $Upload
 * @property Tag $Tag
 */
class Post extends AppModel {

    
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Comment' => array(
			'className' => 'Comment',
			'foreignKey' => 'post_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Upload' => array(
			'className' => 'Upload',
			'foreignKey' => 'post_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Tag' => array(
			'className' => 'Tag',
			'joinTable' => 'posts_tags',
			'foreignKey' => 'post_id',
			'associationForeignKey' => 'tag_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);
    
    public function beforeSave($options = array()) {
        if (!parent::beforeSave($options)) {
          return false;
        }
        if(!isset($this->data['Post']['file'])||$this->data['Post']['file']['error']==4) return true;
        return $this->_processFile();
    }
    
    protected function _processFile() {
        $file = $this->data['Post']['file'];
        
        //debug($this->data);
        if ($file['error'] === UPLOAD_ERR_OK) {
          $name = $file['name'];
          $path = WWW_ROOT . 'files' . DS . $name;
          //move_uploaded_file($file['tmp_name'], $path);
          if (is_uploaded_file($file['tmp_name'])
            && move_uploaded_file($file['tmp_name'], $path)
          ) {
            $this->data['Post']['filename'] = $file['name'];
            $this->data['Post']['size'] = $file['size'];
            $this->data['Post']['mime'] = $file['type'];
            if($file['size']!=0){
                $this->data['Post']['path'] = '/files/' . $name;
            }else{
                $this->data['Post']['path'] ='';
            }
            unset($this->data['Post']['file']);
            return true;
          }
        }
        
        return false;
   }

}
