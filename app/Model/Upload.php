<?php
App::uses('AppModel', 'Model');
/**
 * Upload Model
 *
 * @property Post $Post
 */
class Upload extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Post' => array(
			'className' => 'Post',
			'foreignKey' => 'post_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
        
//        var $actsAs = array(
//            'MeioUpload.MeioUpload' => array('filename')
//        );
        
        public function beforeSave($options = array()) {
            if (!parent::beforeSave($options)) {
              return false;
            }
            return $this->_processFile();
        }
        
        protected function _processFile() {
            $file = $this->data['Upload']['file'];
            if ($file['error'] === UPLOAD_ERR_OK) {
              $name = $file['name'];
              $path = WWW_ROOT . 'files' . DS . $name;
              //move_uploaded_file($file['tmp_name'], $path);
              if (is_uploaded_file($file['tmp_name'])
                && move_uploaded_file($file['tmp_name'], $path)
              ) {
                $this->data['Upload']['name'] = $file['name'];
                $this->data['Upload']['size'] = $file['size'];
                $this->data['Upload']['mime'] = $file['type'];
                $this->data['Upload']['path'] = '/files/' . $name;
                unset($this->data['Upload']['file']);
                return true;
              }
            }
            return false;
        }
}
