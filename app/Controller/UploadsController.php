<?php
App::uses('AppController', 'Controller');
/**
 * Uploads Controller
 *
 * @property Upload $Upload
 * @property PaginatorComponent $Paginator
 */
class UploadsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	
        
        public function index() {
		$this->Upload->recursive = 0;
		$this->set('uploads', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Upload->exists($id)) {
			throw new NotFoundException(__('Invalid upload'));
		}
		$options = array('conditions' => array('Upload.' . $this->Upload->primaryKey => $id));
		$this->set('upload', $this->Upload->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	
        
        public function upload($post_id = null) {
		if ($this->request->is('post')) {
			$this->Upload->create();
                        /////////////////////////////////////////////////
//                        $file = $this->data['Upload']['file'];
//                        if ($file['error'] === UPLOAD_ERR_OK) {
//                          $name = md5($file['name']);
//                          echo $path = WWW_ROOT . 'files' . DS . $name;
//                          if (is_uploaded_file($file['tmp_name'])&& move_uploaded_file($file['tmp_name'], $path))
//                          {
//                            $this->data['Upload']['name'] = $file['name'];
//                            $this->data['Upload']['size'] = $file['size'];
//                            $this->data['Upload']['mime'] = $file['type'];
//                            $this->data['Upload']['path'] = '/files/' . $name;
//                            unset($this->data['Upload']['file']);
//                            return true;
//                          }
//                        }
//                        
                        ////////////////////////////////////////////////
                        $this->request->data['Upload']['post_id']=$post_id;
			if ($this->Upload->save($this->request->data)) {
				$this->Session->setFlash(__('The upload has been saved.'));
                                debug($this->data);
				return;// $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Could not upload file. Please, try again.'));
			}
		}
		$posts = $this->Upload->Post->find('list');
		$this->set(compact('posts'));
	}
        
        

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Upload->exists($id)) {
			throw new NotFoundException(__('Invalid upload'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Upload->save($this->request->data)) {
				$this->Session->setFlash(__('The upload has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The upload could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Upload.' . $this->Upload->primaryKey => $id));
			$this->request->data = $this->Upload->find('first', $options);
		}
		$posts = $this->Upload->Post->find('list');
		$this->set(compact('posts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Upload->id = $id;
		if (!$this->Upload->exists()) {
			throw new NotFoundException(__('Invalid upload'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Upload->delete()) {
			$this->Session->setFlash(__('The upload has been deleted.'));
		} else {
			$this->Session->setFlash(__('The upload could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
