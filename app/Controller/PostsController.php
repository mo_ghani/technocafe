<?php
App::uses('AppController', 'Controller');
/**
 * Posts Controller
 *
 * @property Post $Post
 * @property PaginatorComponent $Paginator
 */
class PostsController extends AppController {

/**
 * Components
 *
 * @var array
 */
    //public $helpers = array('Js' => array('prototype'));
    public $paginate = array(
        'limit' => 10,
    	'order' => array('Post.created' => 'desc' ) 
    );
    
    public $components = array('Paginator','RequestHandler');

/**
 * index method
 *
 * @return void
 */
    
	public function index() {
		//$this->Post->recursive = 0;
            //$this->Paginator->settings
            $this->paginate = array(
                'order' => array('Post.created' => 'desc'),
                'limit' => 10
            );
            $posts = $this->paginate('Post');
            $this->set(compact('posts'));
	}
        
        public function myposts($user_id=null) {
		//$this->Post->recursive = 0;
            if (!$this->Post->exists($user_id)) {
                $this->Session->setFlash(__('Sorry, You have not posted any problem!'));
            }
            $this->Paginator->settings = array(
                'conditions' => array('Post.user_id' => $user_id),
                'order' => array('Post.created' => 'desc'),
                'limit' => 10
            );
            $posts = $this->Paginator->paginate('Post');
            $this->set(compact('posts'));
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
		$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
		$this->set('post', $this->Post->find('first', $options));
                //echo $this->requestAction('/comments/add');
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
            
//            if($this->isAuthorized($this->Auth->user)){
                if ($this->request->is('post')) {
                            $this->Post->create();
                            //debug($this->request->data);
                            $this->request->data['Post']['user_id']=  $this->Auth->user('id');
                            if ($this->Post->save($this->request->data)) {
                                    $this->Session->setFlash(__('The post has been saved.'));
                                    return $this->redirect(array('action' => 'index'));
                            } else {
                                    $this->Session->setFlash(__('The post could not be saved. Please, try again.'));
                            }
                    }

                    $users = $this->Post->User->find('list');
                    $tags = $this->Post->Tag->find('list');
                    $this->set(compact('users', 'tags'));
//                }
//            else{
//                $this->Session->setFlash(__('You are not authorized to see this page!'));
//                return $this->redirect(array('controller'=>'users','action' => 'login'));
//            }
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Post->exists($id)) {
			throw new NotFoundException(__('Invalid post'));
		}
                //debug($this->request->data);
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Post->save($this->request->data)) {
				$this->Session->setFlash(__('The post has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The post could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
			$this->request->data = $this->Post->find('first', $options);
		}
		$users = $this->Post->User->find('list');
		$tags = $this->Post->Tag->find('list');
		$this->set(compact('users', 'tags'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Post->id = $id;
		if (!$this->Post->exists()) {
			throw new NotFoundException(__('Invalid post'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Post->delete()) {
			$this->Session->setFlash(__('The post has been deleted.'));
		} else {
			$this->Session->setFlash(__('The post could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
        
        public function home(){
            
        }
}
