<?php

class UsersController extends AppController {

	public $paginate = array(
        'limit' => 25,
        'conditions' => array('status' => '1'),
    	'order' => array('User.username' => 'asc' ) 
    );
	
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('login','add','mail');
        
        
    }
	
    public function getUsernameById($id){
        $data = $this->User->findById($id);
        return $data;
    }

    public function login() {
		
		//if already logged-in, redirect
		if($this->Session->check('Auth.User')){
			$this->redirect(array('controller'=>'posts','action' => 'index'));		
		}
		
		// if we get the post information, try to authenticate
		if ($this->request->is('post')) {
                    //debug($this->request->data);
                    if ($this->Auth->login()) {
                            //print_r($this->Auth->user);
                            $this->Session->setFlash(__('Welcome, '. $this->Auth->user('username')));
                            $this->redirect($this->Auth->redirectUrl());
                    } else {
                            $this->Session->setFlash(__('Invalid username or password'));
                    }
		} 
	}

	public function logout() {
		$this->redirect($this->Auth->logout());
	}

    public function index() {		
        //if($this->Auth->user('role')=='admin'){
            if($this->Session->check('Auth.User')){
                if($this->Auth->user('role')!='admin'){
                    $this->Session->setFlash(__('Unauthorized attempt!')); 
                    $this->redirect(array('controller'=>'posts','actions'=>'home'));
                }
            }
            $this->paginate = array(
            'limit' => 5,
            'order' => array('User.username' => 'asc' )
            );
            $users = $this->paginate('User');
            $this->set(compact('users')); 
        //}else{
        //    $this->Auth->authError;
        //}
                                   
    }
    
    public function mail(){
        App::uses('CakeEmail', 'Network/Email');
        $email = new CakeEmail('default');
        //$email->from('nasim789@gmail.com');
        $email->to('nasim789@gmail.com');
        $email->subject('test mail');
        debug($email->send('you got it'));
        die('test');
    }

        public function view($id = null) {
        if (!$this->User->exists($id)) {
                throw new NotFoundException(__('Invalid user'));
        }
        $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
        $this->set('user', $this->User->find('first', $options));
    }


    public function add() {
        if ($this->request->is('post')) {
				
			$this->User->create();
                        //debug($this->request->data);
                        $this->request->data['User']['role']='student';
                        $emailRecvr = $this->request->data['User']['email'];
			if ($this->User->save($this->request->data)) {
                                //echo $lastID = $this->User->id;
        
                                App::uses('CakeEmail', 'Network/Email');
                                $email = new CakeEmail('default');
                                //$email->from('nasim789@gmail.com');
                                $email->to($emailRecvr);
                                $email->subject('Account created');
                                $body = 'Hello '. $this->request->data['User']['username'] .', Welcome to TechnoCafe!';
                                $email->send($body);
           
				$this->Session->setFlash(__('The user has been created'));
				if($this->Session->check('Auth.User')){
                                    $this->redirect(array('action' => 'index'));
                                }else{
                                    $this->redirect(array('action' => 'login'));
                                }
			} else {
				$this->Session->setFlash(__('The user could not be created. Please, try again.'));
			}	
        }
    }

    public function edit($id = null) {
	if($this->Session->check('Auth.User')){
            if($this->Auth->user('role')!='admin'){
                $this->Session->setFlash(__('Unauthorized attempt!')); 
                $this->redirect(array('controller'=>'posts','actions'=>'home'));
            }
        }	    
        if (!$id) {
                $this->Session->setFlash('Please provide a user id');
                $this->redirect(array('action'=>'index'));
        }

        $user = $this->User->findById($id);
        if (!$user) {
                $this->Session->setFlash('Invalid User ID Provided');
                $this->redirect(array('action'=>'index'));
        }

        if ($this->request->is('post') || $this->request->is('put')) {
                $this->User->id = $id;
                if ($this->User->save($this->request->data)) {
                        $this->Session->setFlash(__('The user has been updated'));
                        $this->redirect(array('action' => 'edit', $id));
                }else{
                        $this->Session->setFlash(__('Unable to update your user.'));
                }
        }

        if (!$this->request->data) {
                $this->request->data = $user;
        }
    }

    public function delete($id = null) {
		
		if (!$id) {
			$this->Session->setFlash('Please provide a user id');
			$this->redirect(array('action'=>'index'));
		}
		
        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->Session->setFlash('Invalid user id provided');
			$this->redirect(array('action'=>'index'));
        }
        if ($this->User->saveField('status', 0)) {
            $this->Session->setFlash(__('User deleted'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'));
        $this->redirect(array('action' => 'index'));
    }
	
	public function activate($id = null) {
		
		if (!$id) {
			$this->Session->setFlash('Please provide a user id');
			$this->redirect(array('action'=>'index'));
		}
		
        $this->User->id = $id;
        if (!$this->User->exists()) {
            $this->Session->setFlash('Invalid user id provided');
			$this->redirect(array('action'=>'index'));
        }
        if ($this->User->saveField('status', 1)) {
            $this->Session->setFlash(__('User re-activated'));
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not re-activated'));
        $this->redirect(array('action' => 'index'));
    }

}

?>